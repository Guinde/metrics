import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IMetrics } from '../interfaces/metrics.interface';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {

  @Input() data: IMetrics[];
  @Output() selectedMetric: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public onSelectColumnMetric(name):void {
    this.selectedMetric.emit(name);
  }

}
