import { Component } from '@angular/core';
import { IMetrics } from './interfaces/metrics.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title: string = 'metrics';

  public data: IMetrics[] = [
    {
      name: 'Metric 1',
      metrics: [22, 34, 10]
    },
    {
      name: 'Metric 2',
      metrics: [12, 15, 15]
    },
    {
      name: 'Metric 3',
      metrics: [20, 1, 20]
    }
  ] 

  public selectMetric(name): void {
    const metric = this.data.filter(m => m.name === name)[0];
    this.sortMetric(metric.metrics);
  }

  private sortMetric(metrics): number[] {
    metrics.sort((a, b) => a - b);
    return metrics;
  }
}
